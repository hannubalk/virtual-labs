﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Threading;

namespace VirtualLabsControls
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ConfigureESXi : VirtualLabsControl
    {
        Labs.VMwareServerConfig config;
        Labs.ILab lab;
        //public bool Validated { get; set; }
        public ConfigureESXi(Labs.VMwareServerConfig config, Labs.ILab lab)
        {
            InitializeComponent();
            this.config = config;
            this.lab = lab;
            Validated = false;
        }

        private void ButtonValidate_Click(object sender, RoutedEventArgs e)
        {
            StackPanel sp = new StackPanel();
            Label l = new Label() { Content = "Please wait..." };
            sp.Children.Add(l);
            ProgressRing pr = new ProgressRing();
            pr.IsActive = true;
            sp.Children.Add(pr);
            Object oldContent = this.Content;
            this.Content = sp;
            System.Threading.CancellationToken token = new System.Threading.CancellationToken();
            TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            Exception exception = null;
            config.Address = TextBoxAddress.Text;
            config.Username = TextBoxUsername.Text;
            config.Password = PasswordBoxPassword.Password;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    lab.Validate(config);
                }catch(Exception ex)
                {
                    exception = ex;
                }
            }).ContinueWith(s =>
            {
                this.Content = oldContent;
                if (exception != null)
                    MessageBox.Show(exception.Message, "Validation error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                else
                {
                    this.Validated = true;
                    TextBoxAddress.IsEnabled = false;
                    TextBoxUsername.IsEnabled = false;
                    PasswordBoxPassword.IsEnabled = false;
                    ButtonValidate.IsEnabled = false;
                }
            }, token, TaskContinuationOptions.LongRunning, scheduler);
        }

        public void Validate()
        {
            config.Address = TextBoxAddress.Text;
            config.Username = TextBoxUsername.Text;
            config.Password = PasswordBoxPassword.Password;
            try
            {
                lab.Validate(config);
            }catch(Exception ex)
            {
                throw ex;
            }
            
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            TextBoxUsername.Text = "";
            TextBoxAddress.Text = "";
            PasswordBoxPassword.Password = "";
            TextBoxAddress.IsEnabled = true;
            TextBoxUsername.IsEnabled = true;
            PasswordBoxPassword.IsEnabled = true;
            ButtonValidate.IsEnabled = true;
        }

        private void TextBoxAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validated = false;
        }

        private void TextBoxUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validated = false;
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Validated = false;
        }
    }
}
