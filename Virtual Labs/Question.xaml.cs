﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Labs;

namespace VirtualLabs.balk.fi
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : MetroWindow
    {
        Labs.ILab lab;
        List<Page> pages = new List<Page>();
        int currentPage = 0;
        public Question(Labs.ILab lab)
        {
            InitializeComponent();
            this.lab = lab;
            foreach(LabQuestion labQuestion in this.lab.Question)
            {
                if (labQuestion.QuestionIsFile)
                {
                    RichTextBox rtb = new RichTextBox();
                    rtb.Document = new FlowDocument();
                    TextRange tr = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
                    using (FileStream fstream = new FileStream(labQuestion.Question, FileMode.Open))
                    {
                        tr.Load(fstream, System.Windows.DataFormats.Rtf);
                    }
                    rtb.IsReadOnly = true;
                    rtb.IsReadOnlyCaretVisible = false;
                    rtb.BorderThickness = new Thickness(0);
                    Page p = new Page();
                    p.Content = rtb;
                    pages.Add(p);
                }
                else
                {
                    Page p = new Page();
                    StackPanel sp = new StackPanel();
                    sp.Children.Add(new Label() { Content = labQuestion.Question });
                    p.Content = p;
                }
            }
            if (pages.Count == 0)
                pages.Add(new Page());
            FrameQuestion.Content = pages[0];
            ValidatePages();
        }

        private void ValidatePages()
        {
            if (currentPage > 0)
                ButtonPrevious.IsEnabled = true;
            else
                ButtonPrevious.IsEnabled = false;
            if (currentPage < pages.Count() - 1)
                ButtonNext.IsEnabled = true;
            else
                ButtonNext.IsEnabled = false;

        }

        private void ButtonFinish_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Ready to finish?", "End lab?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if(mbr == MessageBoxResult.Yes)
            {
                Object oldcontent = FrameQuestion.Content;
                ButtonPrevious.IsEnabled = false;
                ButtonNext.IsEnabled = false;
                ButtonFinish.IsEnabled = false;
                StackPanel sp = new StackPanel();
                Label l = new Label() { Content = "Please wait..." };
                sp.Children.Add(l);
                ProgressRing pr = new ProgressRing();
                pr.IsActive = true;
                sp.Children.Add(pr);
                FrameQuestion.Content = sp;

                System.Threading.CancellationToken token = new System.Threading.CancellationToken();
                TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();
                Task.Factory.StartNew(() => lab.Evaluate()).ContinueWith(s =>
                    {
                        FrameQuestion.Content = oldcontent;
                        ButtonFinish.IsEnabled = true;
                        ValidatePages();
                    }, token, TaskContinuationOptions.LongRunning, scheduler);
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if(currentPage<pages.Count()-1)
            {
                currentPage++;
                FrameQuestion.Content = pages[currentPage];

            }
            ValidatePages();
        }

        private void ButtonPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (currentPage > 0)
            {
                currentPage--;
                FrameQuestion.Content = pages[currentPage];
            }
            ValidatePages();
        }
    }
}
