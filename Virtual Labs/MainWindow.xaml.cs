﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.IO;
using System.Reflection;



namespace VirtualLabs.balk.fi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private Labs.ILab selectedLab=null;
        List<LabPlugin> labPlugins=null;
        public MainWindow()
        {
            InitializeComponent();

            /* Load Labs */
            LoadPlugins();
            foreach(LabPlugin plugin in labPlugins)
            {
                bool found = false;
                TreeViewItem tvi_dir=null;
                foreach(TreeViewItem tvi in TreeViewLabs.Items)
                {
                    if (tvi.Header.Equals(plugin.Directory))
                    {
                        found = true;
                        tvi_dir = tvi;
                        break;
                    }
                }
                if(found==false)
                {
                    tvi_dir = new TreeViewItem();
                    tvi_dir.Header = plugin.Directory;
                    TreeViewLabs.Items.Add(tvi_dir);
                }
                TreeViewItem item = new TreeViewItem();
                item.Header = plugin.Name;
                item.Tag = plugin.Lab.GetHashCode();
                tvi_dir.Items.Add(item);
            }
            ButtonStart.IsEnabled = false;
        }

        private void LoadPlugins()
        {
            labPlugins = new List<LabPlugin>();
            foreach (String dir in Directory.GetDirectories(new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory + "\\Labs"))
            {
                foreach (string file in Directory.GetFiles(dir, "*.dll"))
                {
                    
                    AssemblyName an = AssemblyName.GetAssemblyName(file);
                    Assembly a = Assembly.Load(an);
                    ICollection<Type> pluginTypes = new List<Type>();
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.IsAbstract || t.IsInterface)
                            continue;
                        else
                        {
                            if (t.GetInterface(typeof(Labs.ILab).FullName) != null)
                            {
                                LabPlugin plugin = new LabPlugin();
                                plugin.Directory = new DirectoryInfo(dir).Name;
                                plugin.Lab = (Labs.ILab)Activator.CreateInstance(t);
                                plugin.Name = plugin.Lab.Name;
                                labPlugins.Add(plugin);
                            }
                        }
                    }
                }
            }
        }

        private void TreeViewLabs_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem tvi = (TreeViewItem)e.NewValue;
            if(tvi.Tag!=null)
            {
                Labs.ILab lab = labPlugins.Single(s => s.Name== (string)tvi.Header).Lab;
                selectedLab = lab;
                ButtonStart.IsEnabled = true; 
                TextRange tr = new TextRange(richTextBoxDescription.Document.ContentStart, richTextBoxDescription.Document.ContentEnd);
                using (FileStream fstream = new FileStream(lab.DescriptionFile, FileMode.Open))
                {
                    tr.Load(fstream, System.Windows.DataFormats.Rtf);
                }
            }
            else
            {
                ButtonStart.IsEnabled = false;
            }
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            Configure config = new Configure(selectedLab);
            config.Show();
            selectedLab.Start();
        }

    }
    public class LabPlugin
    {
        string directory;

        public string Directory
        {
            get { return directory; }
            set { directory = value; }
        }
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        Labs.ILab lab;

        public Labs.ILab Lab
        {
          get { return lab; }
          set { lab = value; }
        }
    }
}
