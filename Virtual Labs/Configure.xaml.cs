﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VirtualLabs.balk.fi
{
    /// <summary>
    /// Interaction logic for Configure.xaml
    /// </summary>
    public partial class Configure : MetroWindow
    {
        Labs.ILab lab;
        public Configure(Labs.ILab lab)
        {
            InitializeComponent();
            this.lab = lab;
            foreach(Labs.LabConfig lc in lab.GetConfigurations())
            {
                TabItem ti = new TabItem()
                {
                    Header = lc.ConfigurationItemName
                };
                switch(lc.GetType().Name)
                {
                    case "LabConfig":
                        break;
                    case "VCenterServerConfig":
                        try
                        {
                            //ConfigureVMwareServer((Labs.VMwareServerConfig)lc, ti);
                            ti.Content = new VirtualLabsControls.ConfigureVCenter((Labs.VMwareServerConfig)lc, lab);
                        }
                        catch (Exception ex) { throw ex; }
                        break;
                    case "ESXiServerConfig":
                        try
                        {
                            ti.Content = new VirtualLabsControls.ConfigureESXi((Labs.VMwareServerConfig)lc, lab);
                        }
                        catch (Exception ex) { throw ex; }
                        break;
                    default:
                        throw new ArgumentException(lc.GetType().Name + " is invalid configuration type");
                }
                TabControlConfigure.Items.Add(ti);
            }
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            StackPanel sp = new StackPanel();
            Label l = new Label() { Content = "Please wait..." };
            sp.Children.Add(l);
            ProgressRing pr = new ProgressRing();
            pr.IsActive = true;
            sp.Children.Add(pr);
            object oldContent = this.Content;
            this.Content = sp;
            bool ok = true;

            System.Threading.CancellationToken token = new System.Threading.CancellationToken();
            TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            List<VirtualLabsControls.VirtualLabsControl> vlcs = new List<VirtualLabsControls.VirtualLabsControl>();
            foreach(TabItem ti in TabControlConfigure.Items)
            {
                VirtualLabsControls.VirtualLabsControl vlc = (VirtualLabsControls.VirtualLabsControl)ti.Content;
                vlcs.Add(vlc);
            }
            Task.Factory.StartNew(() =>
                {
                    foreach (VirtualLabsControls.VirtualLabsControl vlc in vlcs)
                    {
                        
                        if (vlc.Validated == false)
                        {
                            Type t = vlc.GetType();
                           // TabControlConfigure.SelectedIndex = vlcs.IndexOf(vlc);
                            switch (t.FullName)
                            {
                                case "VirtualLabsControls.ConfigureVCenter":
                                    this.Dispatcher.Invoke((Action)(()=>{
                                        try { ((VirtualLabsControls.ConfigureVCenter)vlc).Validate(); }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message, vlc.Name, MessageBoxButton.OK, MessageBoxImage.Warning);
                                            ok = false;
                                        }
                                    }));
                                    break;
                                case "VirtualLabsControls.ConfigureESXi":
                                    this.Dispatcher.Invoke((Action)(() =>
                                    {
                                        try { ((VirtualLabsControls.ConfigureESXi)vlc).Validate(); }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message, vlc.Name, MessageBoxButton.OK, MessageBoxImage.Warning);
                                            ok = false;
                                        }
                                    }));
                                    
                                    break;
                                default:
                                    throw new ArgumentException("Unknown configuration " + t.FullName);
                            }
                            if (vlc.Validated == false)
                                ok = false;
                        }
                    }
                }).ContinueWith(s =>
            {
                this.Content = oldContent;
                if (ok)
                {
                    this.Close();
                    Question q = new Question(lab);
                    q.Show();
                }
            }, token, TaskContinuationOptions.LongRunning, scheduler);
        }
    }
}
