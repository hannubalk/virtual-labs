﻿using Labs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VCALabs
{
    class Lab1NoValidation : ILab
    {
        Lab1 lab = new VCALabs.Lab1();


        public string Name
        {
            get { return lab.Name + " without validation"; }
        }

        public string DescriptionFile
        {
            get { return lab.DescriptionFile; }
        }

        public LabQuestion[] Question
        {
            get { return lab.Question; }
        }

        public void Start()
        {
            lab.Start();
        }

        public LabConfig[] GetConfigurations()
        {
            return lab.GetConfigurations();
        }

        public void SetConfigurations(LabConfig[] configs)
        {
            lab.SetConfigurations(configs);
        }

        public bool Validate(LabConfig lc)
        {
            try
            {
                lab.Validate(lc);
            }
            catch
            { }
            return true;
        }

        public bool Evaluate()
        {
            return lab.Evaluate();
        }
    }
}
