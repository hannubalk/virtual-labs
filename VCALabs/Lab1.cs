﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMware.Vim;
using Labs;

namespace VCALabs
{
    public class Lab1 : Labs.ILab
    {
        private string name = "Lab 1 - Setup";
        private Labs.LabConfig[] configs = { new Labs.VCenterServerConfig() {ConfigurationItemName="vCenter", Required=true },
                                           new Labs.ESXiServerConfig(){ConfigurationItemName="ESXi server 1#", Required=true}, new Labs.ESXiServerConfig(){ConfigurationItemName="ESXi server 2#", Required=true}};
        
        
        public string Name
        {
            get { return name; }
        }

        public string DescriptionFile
        {
            get { return @"Labs\VCA Labs\BL_Lab 1.rtf"; }
        }

        public void Start()
        {
        }

        public bool Validate(LabConfig labConfig)
        {
            if(labConfig==null)
                throw new ArgumentNullException("labConfig", "Lab configuration parameter is null");
            string type = labConfig.GetType().Name;
            switch(type)
            {
                case "LabConfig":
                    throw new NotImplementedException("LabConfig configuration type is not implemented");
                case "VCenterServerConfig":
                    return ValidateVC((VCenterServerConfig)labConfig);
                case "ESXiServerConfig":
                    return ValidateESXi((ESXiServerConfig)labConfig);
                default:
                    throw new Exception(labConfig.ConfigurationItemName + " type is unknown.");
            }
        }

        private bool ValidateESXi(ESXiServerConfig vMwareServerConfig)
        {
            VMware.Vim.VimClient vc;
            vc = new VimClient();
            vc.Connect(string.Format("https://{0}/sdk", vMwareServerConfig.Address));
            UserSession us = vc.Login(vMwareServerConfig.Username, vMwareServerConfig.Password);
            ManagedObjectReference mor = new ManagedObjectReference() { Type = "ServiceInstance", Value = "ServiceInstance" };
            ServiceInstance service = new ServiceInstance(vc, mor);
            if (vc.ServiceContent.About.ApiType != "HostAgent")
                throw new Exception("Please connect to ESXi instead of vCenter or other VMware API product.");
            vc.Disconnect();
            return true;
        }

        private bool ValidateVC(VCenterServerConfig vCenterConfig)
        {
            VMware.Vim.VimClient vc;
            vc = new VimClient();
            vc.Connect(string.Format("https://{0}/sdk", vCenterConfig.Address));
            UserSession us = vc.Login(vCenterConfig.Username, vCenterConfig.Password);
            ManagedObjectReference mor = new ManagedObjectReference() { Type = "ServiceInstance", Value = "ServiceInstance" };
            ServiceInstance service = new ServiceInstance(vc, mor);
            if (vc.ServiceContent.About.ApiType != "VirtualCenter")
                throw new Exception("Please connect to vCenter instead of ESXi host or other VMware API product.");
            List<EntityViewBase> dc_views = vc.FindEntityViews(typeof(Datacenter), null, null, null);
            
            bool IsError = false;
            string error = "";
            if (dc_views == null)
                return true;
            if(dc_views.Count()>0)
            {
                error = "Please remove following datacenters from vCenter:\n";
                dc_views.ForEach((s) =>
                {
                    error += ((Datacenter)s).Name + "\n";
                });
                IsError = true;
            }
            List<EntityViewBase> host_views = vc.FindEntityViews(typeof(HostSystem), null, null, null);
            if(host_views.Count()>0)
            {
                error += "Please remove following hosts from vCenter:\n";
                host_views.ForEach((s) =>
                    {
                        error += ((HostSystem)s).Name + "\n";
                    });
            }
            if (IsError)
                throw new Exception(error);
            vc.Disconnect();
            return true;
        }



        public Labs.LabConfig[] GetConfigurations()
        {
            return configs;
        }

        public void SetConfigurations(Labs.LabConfig[] configs)
        {
            this.configs = configs;
        }


        public LabQuestion[] Question
        {
            get { LabQuestion[] files = {new LabQuestion(){Question="BL_Lab1_Q1.rtf", QuestionIsFile=true, EvaluationItems={new LabQuestionEvaluation(){Text="Datacenter created"}, new LabQuestionEvaluation(){Text="Folders created"}, new LabQuestionEvaluation(){Text="Hosts joined to vCenter"}}},new LabQuestion(){ Question="BL_Lab1_Q2.rtf", QuestionIsFile=true} }; return files; }
        }

        public bool Evaluate()
        {
            //Connect to vCenter
            VCenterServerConfig vCenterConfig = (VCenterServerConfig)configs[0];
            VMware.Vim.VimClient vc;
            vc = new VimClient();
            vc.Connect(string.Format("https://{0}/sdk", vCenterConfig.Address));
            UserSession us = vc.Login(vCenterConfig.Username, vCenterConfig.Password);
            ManagedObjectReference mor = new ManagedObjectReference() { Type = "ServiceInstance", Value = "ServiceInstance" };

            //Evaluate Q1


            //Evaluate Q2


            
            /*
            try
            {
                VCenterServerConfig vCenterConfig = (VCenterServerConfig)configs[0];
                VMware.Vim.VimClient vc;
                vc = new VimClient();
                vc.Connect(string.Format("https://{0}/sdk", vCenterConfig.Address));
                UserSession us = vc.Login(vCenterConfig.Username, vCenterConfig.Password);
                ManagedObjectReference mor = new ManagedObjectReference() { Type = "ServiceInstance", Value = "ServiceInstance" };
                ServiceInstance service = new ServiceInstance(vc, mor);
                if (vc.ServiceContent.About.ApiType != "VirtualCenter")
                    throw new Exception("Please connect to vCenter instead of ESXi host or other VMware API product.");
                NameValueCollection filter = new NameValueCollection();
                filter.Add("name", "Virtual Labs");
                Datacenter datacenter = (Datacenter)vc.FindEntityView(typeof(Datacenter), null, filter, null);
                if (datacenter == null)
                {
                    throw new Exception("Datacenter \"Virtual Labs\" missing from vCenter.");
                }
                Folder f = (Folder)vc.GetView(datacenter.HostFolder, null); //Get folders in datacenter

                ComputeResource cr = (ComputeResource)vc.GetView(f.ChildEntity[0], null);
                List<ViewBase> hosts = vc.GetViewsByMorefs(cr.Host, null);
                foreach (ViewBase vb in hosts)
                {
                    HostSystem hs = (HostSystem)vb;
                }
            }catch(Exception ex)
            {
                throw ex;
            }
             * */
            return true;
        }
    }
}
