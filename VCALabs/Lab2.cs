﻿using Labs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VCALabs
{
    class Lab2 : ILab
    {
        LabConfig[] configs;
        public string Name
        {
            get { return "Lab 2 - Network"; }
        }

        public string DescriptionFile
        {
            get { return @"Labs\VCA Labs\BL_Lab 2.rtf"; }
        }

        public void Start()
        {
            
        }

        public void Validate()
        {
            Thread.Sleep(5000);
            return ;
        }

        public LabConfig[] GetConfigurations()
        {
            LabConfig[] configs = {new ESXiServerConfig(){ConfigurationItemName="", Required=true}};
            return configs;
        }

        public void SetConfigurations(LabConfig[] configs)
        {
            this.configs = configs;
        }


        public bool Validate(LabConfig lc)
        {
            Thread.Sleep(5000);
            return true;
        }


        public LabQuestion[] Question
        {
            get { LabQuestion[] files = {new LabQuestion(){Question="BL_Lab1_Q1.rtf", QuestionIsFile=true} ,new LabQuestion(){Question="BL_Lab1_Q2.rtf", QuestionIsFile=true}}; return files; }
        }

        public bool Evaluate()
        {
            Thread.Sleep(15000);
            return true;
        }
    }
}
