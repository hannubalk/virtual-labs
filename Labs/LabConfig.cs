﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs
{
    public class LabConfig
    {
        private string configurationItemName;

        public string ConfigurationItemName
        {
            get { return configurationItemName; }
            set { configurationItemName = value; }
        }

        private bool required;

        public bool Required
        {
            get { return required; }
            set { required = value; }
        }
    }

    public class ESXiServerConfig : VMwareServerConfig
    {

    }

    public class VCenterServerConfig : VMwareServerConfig
    {

    }

    public class VMwareServerConfig : LabConfig
    {
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    }
}
