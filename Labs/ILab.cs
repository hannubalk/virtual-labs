﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs
{
    public interface ILab
    {
        string Name { get; }
        string DescriptionFile { get; }
        LabQuestion[] Question { get; }
        void Start();
        LabConfig[] GetConfigurations();
        void SetConfigurations(LabConfig[] configs);
        bool Validate(LabConfig lc);
        bool Evaluate();
    }

    public class LabQuestion
    {
        public bool QuestionIsFile {get;set;}
        public string Question { get; set; }
        public int MaxPoints { get; set; }
        public bool Evaluated { get; set; }
        public LabQuestionEvaluation[] EvaluationItems { get; set; }
    }

    public class LabQuestionEvaluation
    {
        public string Text { get; set; }
        public bool Correct { get; set; }

    }
}
